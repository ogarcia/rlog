# rlog

The simplest possible implementation of a [syslog server][syslog] written in
[Rust][rust].

[syslog]: https://www.rfc-editor.org/rfc/rfc5424
[rust]: https://www.rust-lang.org/

## Installation

### From binary

Simply download latest release from [releases page][releases].

[releases]: https://gitlab.com/ogarcia/rlog/-/releases

### From source

#### Installing Rust

rlog build has been tested with current Rust stable release version. You can
install Rust from your distribution package or [use `rustup`][rustup].
```
rustup default stable
```

If you prefer, you can use the stable version only for install rlog (you
must clone the repository first).
```
rustup override set stable
```

[rustup]: https://rustup.rs/

#### Installing rlog

To build rlog binary simply execute the following commands.
```shell
git clone https://gitlab.com/ogarcia/rlog.git
cd rlog
cargo build --release
```

After build the binary is located in `target/release/rlog`.

### Arch Linux package

rlog is packaged in Arch Linux and can be downloaded from the [AUR][aur].

[aur]: https://aur.archlinux.org/packages/rlog-syslog

## Usage

rlog is configured by environment variables.

| Variable | Used for | Default |
| --- | --- | --- |
| `RLOG_LISTEN_HOST` | Listen host | `0.0.0.0` |
| `RLOG_LISTEN_PORT` | Listen port | `514` |
| `RLOG_LOGS_DIRECTORY` | Directory for storing log files | `/var/log/rlog` |

It is not necessary to configure any of the variables mentioned above as
they all have default values, but to launch rlog using the default port it
has to be done as `root` as it is a privileged port.

rlog will create a new file with name `ip_address.log` (e.g.
`192.168.1.1.log`) for each different connection it receives and store the
received log in it. If the file already exists it will simply append the new
log to the end of the file.
