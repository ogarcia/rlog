use std::env;
use std::fs;
use std::io::prelude::*;
use std::net::UdpSocket;
use std::path::Path;

fn main() {
    let listen_host = env::var("RLOG_LISTEN_HOST").unwrap_or("0.0.0.0".to_string());
    let listen_port = env::var("RLOG_LISTEN_PORT").unwrap_or("514".to_string());
    let logs_directory = env::var("RLOG_LOGS_DIRECTORY").unwrap_or("/var/log/rlog".to_string());

    // Check if logs directory is writable
    let write_test_file = Path::new(&logs_directory).join(".rlog");
    if fs::File::options().create(true).write(true).open(&write_test_file).is_err() {
        println!("Cannot write in {}", &logs_directory);
        std::process::exit(1);
    } else {
        fs::remove_file(write_test_file).ok();
    }

    let socket = UdpSocket::bind(format!("{}:{}", listen_host, listen_port)).expect("Binding to address");
    let mut buf = [0; 2048];

    loop {
        let (number_of_bytes, src_addr) = socket.recv_from(&mut buf).expect("Didn't receive data");
        let data = &mut buf[..number_of_bytes];
        // Removes the PRI part (see: https://www.rfc-editor.org/rfc/rfc5424#section-6.2.1)
        let message = match data.iter().position(|&x| x == 62) {
            Some(pos) => &data[pos + 1..],
            None => data
        };
        // Removes the version part if present (see: https://www.rfc-editor.org/rfc/rfc5424#section-6.2.2)
        let message = if message[1] == 32 {
            &message[2..]
        } else {
            message
        };
        // Adds new line character if message lacks it
        let message = if message.ends_with(&[10]) {
            message.to_vec()
        } else {
            [message, &[10]].concat()
        };
        // Write message to logfile
        let mut buffer = fs::File::options().create(true).append(true).open(Path::new(&logs_directory).join(format!("{}.log", src_addr.ip()))).expect("Accessing logfile");
        buffer.write_all(&message).expect("Writing logfile");
    }
}
